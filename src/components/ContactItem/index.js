import React, { useEffect, memo } from "react";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";
import "./index.scss";

const ContactItem = memo(
  ({ data, handleToggleCheckbox, AddCheckedItemsToArray }) => {
    const { id, first_name, last_name, email, avatar, checked } = data;

    const toggleCheckbox = () => {
      handleToggleCheckbox(id);
    };

    //Handler for when checked state change
    useEffect(() => {
      AddCheckedItemsToArray(checked, id);
    }, [id, checked, AddCheckedItemsToArray]);

    return (
      <div onClick={toggleCheckbox} className="contact-row">
        <div className="contact-row__left">
          <div className="contact-row__left__avatar">
            <Avatar alt={`photo ${first_name}`} src={avatar} />
          </div>
          <div className="contact-row__left__user-info">
            <div>
              <Typography variant="h5">{`${first_name} ${last_name}`}</Typography>
            </div>
            <Typography variant="body2">{email}</Typography>
          </div>
        </div>

        <div className="contact-row__right">
          <Checkbox color="primary" checked={checked} />
        </div>
      </div>
    );
  }
);

export default ContactItem;
