import React from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import "./index.scss";

const Header = () => {
  return (
    <header className="header">
      <Paper elevation={1}>
        <Typography variant="h5">Contacts</Typography>
      </Paper>
    </header>
  );
};

export default Header;
