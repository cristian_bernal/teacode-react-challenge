import React from "react";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import "./index.scss";

const Search = ({ handleOnSearchInputChange }) => {
  return (
    <div className="search">
      <div className="search__icon">
        <SearchIcon />
      </div>
      <InputBase onChange={handleOnSearchInputChange} placeholder="Search…" />
    </div>
  );
};

export default Search;
