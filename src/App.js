import React, { useEffect, useState, useMemo, useCallback } from "react";
import useDebounce from "./components/Hooks/useDebounce";
import Typography from "@material-ui/core/Typography";
import ContactItem from "./components/ContactItem";
import Header from "./components/Header/index.js";
import Search from "./components/Search";
import "./app.scss";
// const api_url = "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";

function App() {
  const [contacts, setContacts] = useState([]);
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const debouncedSearchTerm = useDebounce(searchTerm, 500);
  const contactsChecked = [];

  //Fetch data after component mount
  useEffect(() => {
    const getContacts = () => {
      // In development mode, I configure a proxy in the package.json in order to avoid the CORS issue
      fetch("/users.json")
        .then((res) => {
          if (res.status >= 400) {
            throw new Error("Something went wrong...");
          }
          return res.json();
        })
        .then(
          (data) => {
            const sortedContacts = data.sort((a, b) =>
              a.last_name.localeCompare(b.last_name)
            );
            //Set contacts with a default checked value of false
            setContacts(
              sortedContacts.map((item) => ({
                ...item,
                checked: false,
              }))
            );
            setIsLoaded(true);
          },
          //if there is an error setError to show to the client
          (err) => {
            setError(err);
            setIsLoaded(true);
          }
        );
    };

    getContacts();
  }, []);

  const handleOnSearchInputChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleToggleCheckbox = useCallback(
    (id) => {
      setContacts(
        contacts.map((item) =>
          item.id === id ? { ...item, checked: !item.checked } : item
        )
      );
    },
    [contacts]
  );

  //Keeping track of checked contacts
  const AddCheckedItemsToArray = useCallback((checked, id) => {
    if (checked && !contactsChecked.includes(id)) {
      contactsChecked.push(id);
      console.log(contactsChecked);
    } else if (!checked && contactsChecked.includes(id)) {
      const index = contactsChecked.indexOf(id);
      contactsChecked.splice(index, 1);
      console.log(contactsChecked);
    }
    // eslint-disable-next-line
  }, []);

  //Filter functionality for the search bar
  const filteredContacts = useMemo(() => {
    return contacts.filter((contact) => {
      if (!debouncedSearchTerm) {
        return contact;
      }
      return (
        contact.first_name
          .toLowerCase()
          .includes(debouncedSearchTerm.toLowerCase()) ||
        contact.last_name
          .toLowerCase()
          .includes(debouncedSearchTerm.toLowerCase())
      );
    });
  }, [debouncedSearchTerm, contacts]);

  return (
    <div className="App">
      <Header />
      <Search handleOnSearchInputChange={handleOnSearchInputChange} />
      {!isLoaded && (
        <Typography style={{ textAlign: "center" }} variant="h6">
          Loading...
        </Typography>
      )}
      {error && (
        <Typography style={{ textAlign: "center" }} variant="h6">
          {error.message}
        </Typography>
      )}
      <div className="App__contacts-wrapper">
        {filteredContacts.map((contact) => {
          return (
            <ContactItem
              AddCheckedItemsToArray={AddCheckedItemsToArray}
              handleToggleCheckbox={handleToggleCheckbox}
              key={contact.id}
              data={contact}
            />
          );
        })}
      </div>
    </div>
  );
}

export default App;
