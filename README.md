This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# TeaCode React.js Recruitment Challenge

Develop a single webpage that displays a list of contacts

## live demo:

[https://boring-easley-3eed0e.netlify.app/](https://boring-easley-3eed0e.netlify.app/)

## Getting Started

#### Fork/clone the repo

#### yarn install

#### yarn start

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Build for production

Note: Before running yarn build, make sure to delete the proxy from package.json and in App.js, update the URL in the fetch function

#### yarn build

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!
